---
title: Page two
description: Oh, look. Another one.
published: true
date: 2019-11-11T20:53:04.173Z
tags: 
---

# Page Two
And now for something completely different.

```plantuml
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response
```

@startuml
Alice -> Bob: Authentication Request
Bob --> Alice: Authentication Response

Alice -> Bob: Another authentication Request
Alice <-- Bob: Another authentication Response
@enduml